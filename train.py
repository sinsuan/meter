import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense,Dropout,Conv2D,MaxPooling2D,Flatten
import matplotlib.pyplot as plt
%matplotlib inline
from keras.utils import np_utils
import numpy as np
import pickle

############ create model ############

model = Sequential()

model.add(Conv2D(16, (5,5) , activation="relu", padding="same", data_format="channels_last",input_shape=(28,28,1)))
model.add(MaxPooling2D(pool_size=(2,2), data_format="channels_last"))

model.add(Conv2D(36, (5,5) , activation="relu", padding="same", data_format="channels_last"))
model.add(MaxPooling2D(pool_size=(2,2), data_format="channels_last"))

model.add(Flatten())
model.add(Dense(128, activation="relu"))
model.add(Dropout(0.5))
model.add(Dense(10, activation="softmax"))

model.compile(loss = "categorical_crossentropy",
              optimizer = "adam",
              metrics = ['accuracy'])

############ pre-train ############

(x_train,y_train),(x_test,y_test)=mnist.load_data()

x_train_shaped = x_train.reshape(int(6.0e4),28,28,1).astype("float32")/255
x_test_shaped = x_test.reshape(int(1.0e4),28,28,1).astype("float32")/255
y_train_cat = np_utils.to_categorical(y_train)
y_test_cat = np_utils.to_categorical(y_test)

train_history = model.fit(x = x_train_shaped,
                          y = y_train_cat,
                          validation_split=0.20,
                          epochs=10,
                          batch_size=600,
                          verbose=1)

############ train ############

with open("num_on_meter_20191129_11.txt","rb") as f:
    meter = pickle.load(f) # meter = [x,y]

meter_train = [meter[0].reshape(len(meter[0]),28,28,1).astype("float32")/255,
               np.hstack((np_utils.to_categorical(meter[1]),np.array([[0]]*len(meter[1]))))]

for i in [0,1]:
    meter_train[1] = np.hstack((meter_train[1],np.array([[0]]*len(meter_train[1]))))

train_history = model.fit(x = meter_train[0],
                          y = meter_train[1],
                          validation_split=0.2,
                          epochs=10,
                          batch_size=10,
                          verbose=1)

############ save ############

model.save('try_and_edit_model_CNN_2.h5')